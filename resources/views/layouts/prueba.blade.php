@extends('layouts.app')

@section('content')
    <button id="checkout-button">Realizar Pago</button>

    <script src="https://js.stripe.com/v3/"></script>
    <script>
        var stripe = Stripe('{{ config('services.stripe.key') }}');
        var clientSecret = '{{ $clientSecret }}';

        var button = document.getElementById('checkout-button');
        button.addEventListener('click', function () {
            stripe.confirmCardPayment(clientSecret, {
                payment_method: {
                    card: elements.getElement('card'),
                }
            }).then(function (result) {
                if (result.error) {
                    // Mostrar error al usuario
                } else {
                    // Redirigir o mostrar mensaje de éxito
                    window.location.href = "{{ route('payment.callback') }}";
                }
            });
        });
    </script>
@endsection
