<!DOCTYPE html>
<html>
<head>
    <title>Pago Exitoso</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body {
            background-color: #f8f9fa;
            font-family: Arial, sans-serif;
        }

        .container {
            margin-top: 50px;
        }

        .card {
            border: none;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
            border-radius: 10px;
        }

        .payment-success {
            text-align: center;
            padding: 40px;
            background-color: #28a745;
            color: #fff;
            border-radius: 10px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">¡Pago Exitoso!</h2>
                        <p class="card-text">Tu pago ha sido procesado con éxito. ¡Gracias por tu compra!</p>
                        <div class="payment-success">
                            <h3>¡Pago Confirmado!</h3>
                            <p>Recibirás un correo electrónico con los detalles de tu compra.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
