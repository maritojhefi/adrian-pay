<!doctype html>
<html lang="en">

<head>
    {{-- metadatos --}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Procesar Pago</title>
    {{-- cdns --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
        integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA=="
        crossorigin="anonymous"
        referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('toastify.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/18.2.1/css/intlTelInput.css">
    {{-- styles --}}
    <style>
        :root {
            --color-amarillo: #ffb800;
            --color-plomo: #aeaeae;
        }

        .container-title {
            background-image: url("{{ asset('images/bg-black-curve.png') }}");
            background-size: cover;
            background-repeat: no-repeat;
            padding: 45px 0px 140px 0px;
        }

        .container-title h2 {
            font-family: "Montserrat", sans-serif;
            font-style: normal;
            font-weight: 700;
            font-size: 50px;
            line-height: 55px;
        }

        .container-title h2 span {
            color: var(--color-amarillo);
        }

        .container-title p {
            font-family: "IBM Plex Sans", sans-serif;
            font-style: normal;
            font-weight: 400;
            font-size: 23px;
            line-height: 25px;
            color: var(--color-plomo);
        }

        .container-portada {
            margin-top: -11%;
        }

        .container-portada img {
            width: 55%;
        }

        .subtitlo {
            font-family: "Montserrat", sans-serif;
            font-style: normal;
            font-weight: 700;
            font-size: 21px;
            line-height: 25px;
            color: #1d1d1d;
        }

        .icon-star-subtitulo-fa-sm {
            font-size: 16px;
            margin-bottom: 0.5%;
            margin-right: 1.5%;
            color: var(--color-amarillo);
        }

        ul {
            list-style: none;
            padding-left: 0px;
            margin: 6% 0px 8% 5%;
        }

        .icon-circle-fa-xs {
            font-size: 9px;
            margin-bottom: 0.5%;
            color: var(--color-amarillo);
        }

        .icon-star-fa-xs {
            font-size: 12px;
            margin-bottom: 0.5%;
            color: var(--color-amarillo);
        }

        ul li p {
            font-family: "Montserrat", sans-serif;
            font-style: normal;
            font-weight: 400;
            font-size: 17px;
            line-height: 26px;
            color: rgba(33, 37, 41, 0.8);
        }

        ul li p span {
            font-weight: 700;
        }

        /* formulario */
        .seccion-form {
            margin-top: 9%;
        }

        .seccion-form .titulo-seccion {
            font-family: "Montserrat", sans-serif;
            font-style: normal;
            font-weight: 600;
            font-size: 15px;
            line-height: 2px;
            color: rgba(33, 37, 41, 0.8);
        }

        .intl-tel-input,
        .iti {
            width: 100%;
        }

        /* input tarjeta */
        #card-element {
            border: solid 1.7px #ffffff;
            background-color: #ffffff;
            /* border: solid 1.7px #CACFD2; */
            padding: 9px 6px 9px 6px;
            border-radius: 4px;
        }

        .seccion-form .terminos {
            border: solid 1.7px #ffffff;
            /* border: solid 1.7px #CACFD2; */
            padding: 9px 6px 9px 6px;
            border-radius: 4px;
        }

        .seccion-form .bold-terminos {
            font-family: "Montserrat", sans-serif;
            font-style: normal;
            font-weight: 600;
            font-size: 14px;
            line-height: 20px;
            color: rgba(33, 37, 41, 0.8);
            margin-bottom: 0px;
        }

        .seccion-form .normal-terminos {
            font-family: "Montserrat", sans-serif;
            font-style: normal;
            font-weight: 400;
            font-size: 14px;
            line-height: 20px;
            color: rgba(33, 37, 41, 0.8);
            margin-bottom: 0px;
        }

        .pie-pagina {
            background-color: black;
            padding-top: 4%;
            padding-bottom: 4%;
        }

        .pie-pagina .container-101-digital img {
            height: 60px;
            filter: sepia(0) grayscale(1) brightness(1) blur(0px) contrast(1) invert(0) saturate(1);
            opacity: 0.48;
        }

        .pie-pagina .container-stripe img {
            height: 60px;
            opacity: 0.3;
            filter: sepia(0) grayscale(0) brightness(1) blur(0px) contrast(1) invert(1) saturate(1);
        }

        .pie-pagina .container-paypal img {
            height: 60px;
            filter: sepia(0) grayscale(1) brightness(3) blur(0px) contrast(1) invert(0) saturate(1);
            opacity: 0.3;
        }

        .pie-pagina .container-gogo img {
            height: 60px;
            filter: sepia(0) grayscale(0) brightness(1) blur(0) contrast(1) invert(0) saturate(1);
        }

        #contenedor-formulario {
            background-color: #CACFD2;
        }

        .precio-descuento-img {
            width: 210px;
        }

        @media screen and (max-width: 720px) {
            .container-title h2 {
                font-size: 43px;
            }

            .container-title p {
                font-size: 20px;
            }

            .container-portada {
                margin-top: -24%;
            }

            .container-portada img {
                width: 80%;
            }

            .subtitlo {
                font-size: 19px;
                line-height: 23px;
            }

            .icon-star-subtitulo-fa-sm {
                font-size: 14px;
                margin-bottom: 0.5%;
                margin-right: 1.5%;
                color: var(--color-amarillo);
            }

            ul {
                margin: 4% 0px 6% 3%;
            }

            ul li p {
                font-size: 15px;
                line-height: 24px;
            }

            .conten-padding {
                padding: 0px !important;
            }

            #contenedor-formulario {
                padding: 8px !important;
            }

            .precio-descuento-img {
                width: 200px;
            }
        }

        @media screen and (max-width: 500px) {
            .container-portada {
                margin-top: -36%;
            }

            .container-portada img {
                width: 100%;
            }

            .precio-descuento-img {
                width: 200px;
            }
        }

        #montoPagar::-webkit-inner-spin-button,
        #montoPagar::-webkit-outer-spin-button {
            -webkit-appearance: none;
            appearance: none;
            margin: 0;
            /* Elimina el espacio que ocupa el botón */
        }
    </style>
</head>

<body>

    {{-- header --}}
    <div class="container-fluid p-0">
        <div class="text-center text-white container-title">
            <h2>Complete Su <span>Orden</span></h2>
            <p>Tu producto te está esperando. Completa tu pedido ahora.</p>
        </div>
        <div class="text-center text-white container-portada">
            <img src="{{ asset('images/paquetes.png') }}" alt="paquetes">
        </div>
    </div>

    {{-- body --}}
    <div class="container-md shadow-lg p-3 mb-5 bg-body rounded">
        <div class="row">
            <div class="col-md-6">
                <div class="p-4 conten-padding">
                    <h3 class="subtitlo"><i class="fas fa-star icon-star-subtitulo-fa-sm"></i> Esto Es Lo Que Obtendrás
                    </h3>
                    <ul>
                        <li>
                            <p><i class="fas fa-circle icon-circle-fa-xs"></i> <span>Módulo 1</span> El Poder para hacer
                                las Riquezas 1</p>
                        </li>
                        <li>
                            <p><i class="fas fa-circle icon-circle-fa-xs"></i> <span>Módulo 2</span> El Poder para hacer
                                las Riquezas 2</p>
                        </li>
                        <li>
                            <p><i class="fas fa-circle icon-circle-fa-xs"></i> <span>Módulo 3</span> El Poder para hacer
                                las Riquezas 3</p>
                        </li>
                        <li>
                            <p><i class="fas fa-circle icon-circle-fa-xs"></i> <span>Módulo 4</span> El Poder para hacer
                                las Riquezas 4</p>
                        </li>
                        <li>
                            <p><i class="fas fa-circle icon-circle-fa-xs"></i> <span>Módulo 5</span> Códigos Secretos
                            </p>
                        </li>
                        <li>
                            <p><i class="fas fa-circle icon-circle-fa-xs"></i> <span>Módulo 6</span> Entendiendo el
                                nuevo orden</p>
                        </li>
                        <li>
                            <p><i class="fas fa-circle icon-circle-fa-xs"></i> <span>Módulo 7</span> Entendiendo los
                                CBDCS (Nuevas monedas globales)</p>
                        </li>
                        <li>
                            <p><i class="fas fa-circle icon-circle-fa-xs"></i> <span>Módulo 8</span> Entendiendo el
                                Bitcoin y las Criptomonedas</p>
                        </li>
                        <li>
                            <p><i class="fas fa-circle icon-circle-fa-xs"></i> <span>Bono 1</span> YUAN CHINO</p>
                        </li>
                        <li>
                            <p><i class="fas fa-circle icon-circle-fa-xs"></i> <span>Bono 2</span> Prácticas Reales
                                Bitcoin y Criptomonedas</p>
                        </li>
                    </ul>
                </div>
                <div class="px-4 mt-1 pb-3 conten-padding">
                    <h3 class="subtitlo"><i class="fas fa-star icon-star-subtitulo-fa-sm"></i> Bonificaciones Incluidas
                    </h3>
                    <ul>
                        <li>
                            <p><i class="far fa-star icon-star-fa-xs"></i> <span>Bono 1 Master Class</span> - Yuan Chino
                                y Globalización (Nuevo Orden) - Bricks</p>
                        </li>
                        <li>
                            <p><i class="far fa-star icon-star-fa-xs"></i> <span>Bono 2 Master Class</span> - Bitcoin y
                                las Criptomonedas</p>
                        </li>
                        <li>
                            <p><i class="far fa-star icon-star-fa-xs"></i> <span>Podcast</span> - El poder de tus
                                palabras</p>
                        </li>
                    </ul>
                    <div class="text-center">
                        <img class="precio-descuento-img" src="{{ asset('images/precio-descuento.png') }}"
                            alt="precio con descuento">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div id="contenedor-formulario" class="p-4 conten-padding shadow rounded">
                    <h3 class="subtitlo">Pago con tarjeta</h3>
                    <div class="seccion-form">
                        <p class="titulo-seccion">Información de contacto</p>
                        <div class="input-group">
                            <span class="input-group-text bg-body border-end-0"><i class="far fa-envelope"></i></span>
                            <input type="email" id="correo" class="form-control"
                                placeholder="correoelectronico@ejemplo.com">
                        </div>
                        <div class="input-group">
                            <input type="tel" id="phoneNumber" class="form-control">
                        </div>
                    </div>
                    <div class="seccion-form">
                        <p class="titulo-seccion">Precio en dolares</p>
                        <input type="number" id="montoPagar" class="form-control" onclick="PrimerClickPrecio()" oninput="validarPrecio()"
                            placeholder="Ofrenda desde 9.97">
                    </div>
                    <div class="seccion-form">
                        <p class="titulo-seccion">Información de la tarjera</p>
                        <div id="card-element" class="mb-3 mt-2">
                            <!-- Un espacio reservado donde se insertará el formulario de tarjeta -->
                        </div>
                    </div>
                    <div class="seccion-form">
                        <p class="titulo-seccion">Nombre del titular de la tarjeta</p>
                        <div class="input-group">
                            <input type="text" id="nombre" class="form-control">
                        </div>
                    </div>
                    <div class="seccion-form">
                        <p class="titulo-seccion">Dirección de facturación</p>
                        <div class="input-group">
                            <select id="pais" class="form-select">
                            </select>
                        </div>
                        <div class="input-group">
                            <input type="text" id="direccion1" class="form-control"
                                placeholder="Línea 1 de dirección">
                        </div>
                        <div class="input-group">
                            <input type="text" id="direccion2" class="form-control"
                                placeholder="Línea 2 de dirección">
                        </div>
                        <div class="input-group">
                            <input type="text" id="ciudad" class="form-control" placeholder="Ciudad">
                        </div>
                    </div>
                    <div class="seccion-form">
                        <div class="terminos">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="guardarDatos">
                                <p class="bold-terminos">Guardar mis datos de forma segura para un proceso de compra en
                                    un clic</p>
                                <p class="normal-terminos">Paga más rápido en Disruptive - Gogo Marca LTD y en todos
                                    los comercios que aceptan link.</p>
                            </div>
                        </div>
                    </div>
                    <div class="seccion-form">
                        <div class="d-grid gap-2">
                            <button id="checkout-button" class="btn btn-primary" type="button" data-secret="">
                                Pagar <span id="spinner" class="spinner-border spinner-border-sm" role="status"
                                    aria-hidden="true" style="display: none;"></span>
                            </button>
                        </div>
                    </div>
                    <div class="seccion-form mt-2">
                        <div class="row">
                            <div class="col-4 p-2 text-center">
                                <img src="{{ asset('images/logo-rempolso.png') }}" alt="Logo de rembolso"
                                    width="120">
                            </div>
                            <div class="col-8 ps-3 mt-3 mt-sm-0">
                                <h4 class="bold-terminos text-white mt-4 mt-sm-0 ps-1">Garantía de satisfacción total o
                                    te devolvemos el dinero</h4>
                                <p class="normal-terminos text-white d-none d-sm-block">¡Estamos 100% convencidos que
                                    tendrás excelentes resultados con nuestro curso y te va a encantar! Por eso te damos
                                    una garantía de satisfacción total durante los primeros 14 días o la devolución de
                                    tu dinero.</p>
                            </div>
                            <div class="col-sm-12">
                                <p class="normal-terminos text-white d-block d-sm-none">¡Estamos 100% convencidos que
                                    tendrás excelentes resultados con nuestro curso y te va a encantar! Por eso te damos
                                    una garantía de satisfacción total durante los primeros 14 días o la devolución de
                                    tu dinero.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- footer --}}
    <div class="container-fluid pie-pagina">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="text-center container-101-digital">
                        <img src="{{ asset('images/empresas/101-digital.png') }}" alt="101 DIGITAL">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="text-center container-stripe">
                        <img src="{{ asset('images/empresas/stripe.png') }}" alt="STRIPE">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="text-center container-paypal">
                        <img src="{{ asset('images/empresas/paypal.png') }}" alt="PayPal">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="text-center container-gogo">
                        <img src="{{ asset('images/empresas/gogo.png') }}" alt="GOGO">
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- cdns --}}
    <script src="https://code.jquery.com/jquery-3.7.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/js/all.min.js"
        integrity="sha512-uKQ39gEGiyUJl4AI6L+ekBdGKpGw4xJ55+xyJG7YFlJokPNYegn9KwQ3P8A7aFQAUtUsAQHep+d/lrGqrbPIDQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="{{ asset('toastify-es.js') }}"></script>
    <script src="{{ asset('toastify.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/18.2.1/js/intlTelInput.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/18.2.1/js/intlTelInput-jquery.js"
        integrity="sha512-FhdxuQlB84ZIFiX/84s61yflIV6UOHBY2cCal6yrUjqq8ogAhmp3NwWYRG1t1mnrUabFNGx29ShlYub9AUchSw=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    {{-- scripts --}}
    <script>
        $(document).ready(function() {

            $("#phoneNumber").intlTelInput({
                autoPlaceholder: "agregar país y código de área",
                separateDialCode: true,
                utilsScript: "{{ asset('utils.js') }}"
            });
            var stripe = Stripe('{{ config('services.stripe.key') }}');

            var checkoutButton = document.getElementById('checkout-button');

            function validarCampos() {
                var nombre = $('#nombre').val();
                var correo = $('#correo').val();
                var telefono = $('#phoneNumber').val();
                var pais = $("#pais").val();
                var ciudad = $("#ciudad").val();
                var nombreValido = validarNombre(nombre);
                var correoValido = validarCorreo(correo);
                var telefonoValido = validarTelefono(telefono);
                var montoPagar = $("#montoPagar").val();
                montoPagar = montoPagar.replace(',', '.');
                var montoPagarFormateado = parseFloat(montoPagar);

                if (!correoValido) {
                    showToast('Ingrese un correo válido.', 'orange', 'orange');
                    return false;
                }

                if (!telefonoValido) {
                    showToast('Ingrese un número válido.', 'orange', 'orange');
                    return false;
                }

                if (montoPagarFormateado < 9.97 ||  montoPagar == "") {
                    showToast('El monto a pagar debe ser 9.97 dólares.', 'orange', 'orange');
                    return false;
                }

                let tarjetaVacia = validarCampoTarjeta();
                if (tarjetaVacia == true){
                    showToast('El número de su tarjeta debe ser válido.', 'orange', 'orange');
                    return false;
                }

                if (!nombreValido) {
                    showToast('El nombre no debe estar vacío.', 'orange', 'orange');
                    return false;
                }

                if (pais == "0") {
                    showToast('Seleccione un país.', 'orange', 'orange');
                    return false;
                }

                if (ciudad == "") {
                    showToast('Ingrese el nombre de la ciudad.', 'orange', 'orange');
                    return false;
                }
            }

            checkoutButton.addEventListener('click', function(event) {
                event.preventDefault();

                // Validar los campos
                if (validarCampos() != false) {
                    checkoutButton.disabled = true;
                    actualizarMonto().then(function(retorno) {
                        if (retorno) {
                            // Realiza acciones si la solicitud fue exitosa
                            document.getElementById('spinner').style.display = 'inline-block';
                            stripe.confirmCardPayment(
                                checkoutButton.dataset.secret, {
                                    payment_method: {
                                        card: cardElement,
                                        billing_details: {
                                            name: $('#nombre').val(),
                                            email: $('#correo').val(),
                                        },
                                    }
                                }
                            ).then(function(result) {
                                if (result.error) {
                                    showToast(result.error.message, 'red', 'red')
                                    checkoutButton.disabled = false;
                                    document.getElementById('spinner').style.display =
                                        'none';
                                } else {
                                    // Redireccionar o realizar acciones después del pago exitoso
                                    $.ajax({
                                        type: "post",
                                        url: "{{ route('payment.callback') }}",
                                        data: {
                                            _token: "{{ csrf_token() }}",
                                            correo: $('#correo').val(),
                                            nombre: $('#nombre').val(),
                                            telefono: $('#phoneNumber').val(),
                                            pais: $('#pais').val(),
                                            direccion1: $('#direccion1').val(),
                                            direccion2: $('#direccion2').val(),
                                            ciudad: $('#ciudad').val(),
                                            payment_intent: result.paymentIntent.id
                                        },
                                        success: function(response) {
                                            if (response.pagado) {
                                                conversionOnlineProvely();
                                                showToast(
                                                    '¡El pago se ha realizado exitosamente!',
                                                    'green', 'green');
                                            } else {
                                                showToast(
                                                    'Error de pago. Verifique sus datos e intente de nuevo.',
                                                    'red',
                                                    'red')
                                            }
                                            if (response.suscrito) {
                                                showToast(
                                                    '¡Te has suscrito con éxito!',
                                                    'green',
                                                    'green')
                                            } else {
                                                showToast(
                                                    'Problemas con la suscripción. Verifica tu información con nosotros.',
                                                    'orange',
                                                    'red')
                                            }
                                            checkoutButton.disabled = false;
                                            limpiar();
                                            document.getElementById('spinner')
                                                .style
                                                .display =
                                                'none';
                                            setTimeout(function() {
                                                window.location.href =
                                                    response.url;
                                            }, 500);
                                        },
                                        error: function(response) {
                                            checkoutButton.disabled = false;
                                            document.getElementById('spinner')
                                                .style
                                                .display =
                                                'none';
                                            showToast('Ha ocurrido un error, verifique sus datos.', 'red',
                                                'red')
                                        }
                                    });
                                }
                            });
                        } else {
                            // Realiza acciones si la solicitud falló
                            checkoutButton.disabled = false;
                        }
                    });
                }
            });

            function showToast(mensaje, background1, background2) {
                Toastify({
                    text: mensaje,
                    duration: 5000, // Duración en milisegundos (3 segundos en este caso)
                    close: true, // Puede cerrarse haciendo clic en él
                    gravity: "top", // Posición del toast (top, bottom, left, right)
                    position: "right", // Alineación dentro de la posición (left, center, right)
                    backgroundColor: "linear-gradient(to right," + background1 + "," + background2 +
                        ")", // Color de fondo
                }).showToast();
            }

            // Crea un elemento del formulario de tarjeta
            var elements = stripe.elements();
            var cardElement = elements.create('card');

            function validarNombre(nombre) {
                // Agrega tu lógica de validación aquí
                return nombre.trim() !== '';
            }

            function validarCorreo(correo) {
                // Agrega tu lógica de validación de correo aquí
                var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                return pattern.test(correo);
            }

            function validarTelefono(telefono) {
                // Agrega tu lógica de validación de correo aquí
                return telefono.trim() !== '';
            }


            function validarCampoTarjeta() {
                var displayError = document.getElementById('card-errors');

                // Obtener el valor del elemento de tarjeta
                var cardValue = cardElement._empty ? '' : cardElement._complete;

                if (cardValue === '') {
                    return true;
                } else {
                    return false;
                }
            }

            // Añade el elemento del formulario de tarjeta al elemento con el ID "card-element"
            cardElement.mount('#card-element');

            listarPaises();

            limpiar();
        });

        function actualizarMonto() {
            return new Promise(function(resolve, reject) {
                var montoPagar = $("#montoPagar").val();
                montoPagar = montoPagar.replace(',', '.');
                var montoPagarFormateado = parseFloat(montoPagar);
                var url = '{{ route('payment.reProcesoPago') }}' + '?montoPago=' + montoPagarFormateado;

                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function(response) {
                        if (response == false) {
                            showToast('Ha ocurrido un error, verifique sus datos.', 'orange', 'orange');
                            resolve(false); // Resuelve la promesa con error
                        } else {
                            $("#checkout-button").attr("data-secret", response);
                            resolve(true); // Resuelve la promesa con éxito   
                        }
                    },
                    error: function(err) {
                        showToast('Ha ocurrido un error, verifique sus datos.', 'orange', 'orange');
                        resolve(false); // Resuelve la promesa con error
                    },
                });
            });
        }

        function listarPaises() {
            $.ajax({
                url: "https://restcountries.com/v3.1/all",
                type: "GET",
                dataType: "json",
                success: function(countries) {
                    var countrySelect = $("#pais");

                    var option = $("<option>").text("Seleccionar").val("0");
                    countrySelect.append(option);

                    function compareByName(a, b) {
                        var nameA = a.name.common.toLowerCase();
                        var nameB = b.name.common.toLowerCase();

                        if (nameA < nameB) return -1;
                        if (nameA > nameB) return 1;
                        return 0;
                    }

                    countries.sort(compareByName);

                    $.each(countries, function(index, country) {
                        var countryName = country.name.common;
                        var countryCode = country.cca2;
                        var option = $("<option>").text(countryName).val(countryName);
                        countrySelect.append(option);
                    });
                },
                error: function() {
                    console.log("No se pudo obtener la lista de países.");
                }
            });
        }

        function limpiar() {
            $('#correo').val("");
            $('#nombre').val("");
            $('#phoneNumber').val("");
            $('#direccion1').val("");
            $('#direccion2').val("");
            $('#ciudad').val("");
            $('#montoPagar').val("");
            listarPaises();
        }

        function conversionOnlineProvely() {
            var dataToSend = {
                email: $('#correo').val(),
                first_name: $('#nombre').val(),
                country: $('#pais').val(),
                city: $('#ciudad').val(),
            };

            var webhookURL = "https://app.provely.io/api/webhooks/3ea4677c-c958-46fd-a984-72db578ff33a/custom";
            $.ajax({
                type: "POST",
                url: webhookURL,
                data: JSON.stringify(dataToSend),
                contentType: "application/json",
                success: function(response) {
                    console.log("Solicitud enviada con éxito:", response);
                },
                error: function(error) {
                    console.error("Error al enviar la solicitud:", error);
                }
            });
        }

        let timeoutCliente;
        function validarPrecio() {
            clearTimeout(timeoutCliente);
            timeoutCliente = setTimeout(function() {
                var montoPagar = $("#montoPagar").val();
                montoPagar = montoPagar.replace(',', '.');
                var montoPagarFormateado = parseFloat(montoPagar);
                if (montoPagarFormateado < 9.97 ||  montoPagar == "") {
                    $("#montoPagar").addClass('border border-2 border-danger text-danger');
                } else {
                    $("#montoPagar").removeClass('border border-2 border-danger text-danger');
                }
            }, 1000);
        }

        let primerClick = 0;
        function PrimerClickPrecio(){
            if(primerClick == 0){
                $('#montoPagar').val(9.97);
                primerClick = 1;
                return;
            }
        }
    </script>

    <!-- Start of Provely Notification Display Code -->
    <script>
        (function(w, n) {
            if (typeof(w[n]) == 'undefined') {
                w[n] = [];
                w.provelySet = function() {
                    w[n].push(arguments);
                };
                d = document.createElement('script');
                d.type = 'text/javascript';
                d.defer = 1;
                d.charset = "UTF-8";
                d.src = 'https://provely-public.s3.amazonaws.com/scripts/provely-widget.js?version=1.0';
                x = document.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(d, x);
            }
        })(window, 'provelyObj');
        provelySet('config', 'uuid', '3ea4677c-c958-46fd-a984-72db578ff33a');
        provelySet('config', 'type', 'notification');
    </script>
    <!-- End of Provely Notification Display Code -->
</body>

</html>
