<?php

namespace App\Http\Controllers;

use Exception;
use Stripe\Stripe;
use GuzzleHttp\Client;
use Stripe\PaymentIntent;
use Illuminate\Http\Request;
use Stripe\Checkout\Session;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\RequestException;

class PaymentController extends Controller
{
    public function procesarPago()
    {
        return view('payment.initiate');
    }

    public function reProcesarPago(Request $request)
    {
        try {
            $request->validate([
                'montoPago'=> 'required | decimal:0,2 | min:9.97', 
            ]);

            $valorEnDolares = ($request->montoPago * 1000) / 10;

            // Configura la clave secreta de Stripe
            Stripe::setApiKey(config('services.stripe.secret'));
    
            // Crea un PaymentIntent en Stripe
            $paymentIntent = PaymentIntent::create([
                'amount' => $valorEnDolares,  // Monto en centavos
                'currency' => 'usd',
            ]);
            return $paymentIntent->client_secret;

        } catch (\Throwable $th) {
            return false;
        }
    }

    public function pagoCallback(Request $request)
    {
        $pagado = false;
        $suscrito = false;
        if ($request->input('payment_intent')) {
            $pagado = $this->checkPaymentStatus($request->payment_intent);
            $token = $this->refreshAccessToken('2Kk69x4AkWCKQn0GA83eHSSvoeVQ0n8t');
            $addAweber = $this->addToAWeberList($token['access_token'], $request->correo, $request->nombre, $request->telefono, $request->pais, $request->direccion1, $request->direccion2, $request->ciudad);
            if ($addAweber) {
                $suscrito = true;
            }
        }
        $urlRedireccion = "https://adriantrigo.com/gracias-acceso-pphr/";
        return response()->json(['status' => 'success', 'suscrito' => $suscrito, 'pagado' => $pagado, 'url' => $urlRedireccion]);
    }

    public function checkPaymentStatus($paymentIntentId)
    {
        // Configura la clave secreta de Stripe
        Stripe::setApiKey(config('services.stripe.secret'));

        try {
            // Recupera el PaymentIntent desde Stripe
            $paymentIntent = PaymentIntent::retrieve($paymentIntentId);

            // Verifica el estado del PaymentIntent
            if ($paymentIntent->status === 'succeeded') {
                // El pago fue exitoso
                return true;
            } else {
                // El pago no fue exitoso
                return false;
            }
        } catch (\Exception $e) {
            // Maneja cualquier error que pueda ocurrir al consultar Stripe
            return "Hubo un error al verificar el pago: " . $e->getMessage();
        }
    }
    public function getAccessToken()
    {
        $client = new Client();

        $response = $client->post('https://auth.aweber.com/oauth2/token', [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'client_id' => 'DNEFwXOiVU1AKOUidxa0kloux2ze5OCz',
                'client_secret' => 'Aa7DrhjKnDgAXZUuJF3PdAw89WQ0HwCw',
                'code' => 'WCvmiI9PdQrJT418R0Y4C893He3CMqSg',
                'redirect_uri' => 'https://app.disruptive.center/aweber/callback',
            ],
        ]);

        $responseData = json_decode($response->getBody(), true);

        return $responseData;
    }
    public function refreshAccessToken($refreshToken)
    {
        $client = new Client();
        $response = $client->post('https://auth.aweber.com/oauth2/token', [
            'form_params' => [
                'grant_type' => 'refresh_token',
                'client_id' => 'DNEFwXOiVU1AKOUidxa0kloux2ze5OCz',
                'client_secret' => 'Aa7DrhjKnDgAXZUuJF3PdAw89WQ0HwCw',
                'refresh_token' => $refreshToken,
            ],
        ]);
        $responseData = json_decode($response->getBody(), true);
        // Maneja la respuesta como desees
        return $responseData;
    }

    public function addToAWeberList($token, $correo, $nombre, $telefono ,$pais, $direccion1, $direccion2, $ciudad)
    {
        try {
            $client = new Client();
            $accountID = '264120'; // ID CUENTA ADRIAN
            $listID = '6612593'; // ID PODER PARA RIQUEZAS

            $listURL = "/accounts/{$accountID}/lists/{$listID}";

            $response = $client->post("https://api.aweber.com/1.0{$listURL}/subscribers", [
                'headers' => [
                    'Authorization' => "Bearer {$token}",
                    'Content-Type' => 'application/json',
                ],
                'json' => [
                    'email' => "{$correo}",
                    'name' => "{$nombre}",
                    'custom_fields' => ['phone' => $telefono, 'country' => $pais, 'direccion1' => $direccion1, 'direccion2' => $direccion2, 'city' => $ciudad],
                ],
            ]);

            // Maneja la respuesta como desees
            return true;
        } catch (Exception $e) {
            // Maneja el error de la solicitud
            return false;
        }
    }
}
